import 'package:flutter/material.dart';
import 'package:componentes/src/routes.dart'; 
void main() => runApp(MyApp());
 
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Componentes App',
      debugShowCheckedModeBanner: false,
      initialRoute: initialRoute,
      routes: getRoutes(),
      onGenerateRoute: manageRoutes,
    );
  }
}