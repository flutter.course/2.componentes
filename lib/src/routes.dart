import 'package:flutter/material.dart';

import 'package:componentes/src/pages/alerts.page.dart';
import 'package:componentes/src/pages/avatars.page.dart';
import 'package:componentes/src/pages/home.page.dart';
import 'package:componentes/src/pages/cards.page.dart';
import 'package:componentes/src/pages/animated_container.page.dart';
import 'package:componentes/src/pages/inputs.page.dart';

final String initialRoute = '/';

Map<String,WidgetBuilder> getRoutes() {
  return <String,WidgetBuilder>{
    '/': (BuildContext context) => HomePage(),
    'alert': (BuildContext context) => AlertsPage(),
    'avatar': (BuildContext context) => AvatarsPage(),
    'card': (BuildContext context) => CardsPage(),
    'animatedContainer': (BuildContext context) => AnimatedContainerPage(),
    'inputs': (BuildContext context) => InputsPage()
  };
}

MaterialPageRoute manageRoutes(RouteSettings settings){
  print('Ruta: ${settings.name}');
  return MaterialPageRoute(
      builder:(BuildContext context) => AlertsPage()
    );
}