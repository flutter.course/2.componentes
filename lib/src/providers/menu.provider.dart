import 'package:flutter/services.dart' show rootBundle;
import 'dart:convert';

class _MenuProvider {
  List<dynamic> options = [];
  
  Future<List<dynamic>> cargarData() {
    return rootBundle.loadString('data/menu_opts.json')
      .then((rowData) {
        Map dataMap = json.decode(rowData);
        return dataMap['rutas'];
      });
  }
}

final menuProvider = new _MenuProvider();