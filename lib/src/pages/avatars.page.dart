import 'package:flutter/material.dart';

class AvatarsPage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Avatars'),
        actions: [
          Container(
            padding: EdgeInsets.all(3),
            child: CircleAvatar(
              radius: 30,
              backgroundImage: NetworkImage('https://icon-library.com/images/avatar-icon/avatar-icon-6.jpg'),
            ),
          ),
          Container(
            margin: EdgeInsets.only(right: 10),
            child: CircleAvatar(
              child: Text("DC"),
              backgroundColor: Colors.red,
            ),
          )
        ],
      ),
      body: Center(
        child: FadeInImage(
          placeholder: AssetImage('assets/loading.gif'), 
          image: NetworkImage('https://icon-library.com/images/avatar-icon/avatar-icon-6.jpg'),
          fadeInDuration: Duration(milliseconds: 200),
        )
      ),
    );
  }
}