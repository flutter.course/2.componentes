import 'package:flutter/material.dart';

class InputsPage extends StatefulWidget {
  @override
  _InputsPageState createState() => _InputsPageState();
}

class _InputsPageState extends State<InputsPage> {

  bool editing = false;  
  var personForm = <String,TextEditingController>{
    "name" : TextEditingController(),
    "email" : TextEditingController(),
    "password" : TextEditingController(),
  };

  var users = <Map<String,String>>[
  ];

  BuildContext context ;
  
  @override
  Widget build(BuildContext context) {
    this.context = context;
    return Scaffold(
      appBar: AppBar(
        title: Text('Inputs'),
      ),
      body: ListView(
        padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 20.0),
        children: [
          _createNameInput(),
          Divider(),
          _createEmailInput(),
          Divider(),
          _createPasswordInput(),
          Divider(),
          _createPerson(
            name: personForm['name'].text, 
            email: personForm['email'].text, 
            password: personForm['password'].text
          ),
          SizedBox(height:20.0),
          Text(
            "Registered Users",
            style: TextStyle(
              fontSize: 24.0,
              fontWeight: FontWeight.bold
            ),
          ),
          SizedBox(height:20.0),
          Container(
            child: Column(
              children: _renderUsers()
            )
          )
          
        ],
      ),
    );
  }

  void clearInputs (){
    setState(() {
      personForm['name'].text = ""; 
      personForm['email'].text = ""; 
      personForm['password'].text = "";
    });
  }

  Widget _createNameInput() => TextField(
    controller: personForm['name'],
    autofocus: true,
    textCapitalization: TextCapitalization.sentences,
    decoration: InputDecoration(
      counter: Text('${personForm["name"].text.length} Characters'),
      hintText: 'Name',
      labelText: 'Name',
      helperText: 'Just set the name',
      suffixIcon: Icon(Icons.accessibility),
      icon: Icon(Icons.account_circle),
      border: OutlineInputBorder(
        borderRadius: BorderRadius.circular(20.0)
      )
    ),
    onChanged: (value) {
      setState(() {
      });
    },
  );

  Widget _createEmailInput() => TextField(
    controller: personForm['email'],
    keyboardType: TextInputType.emailAddress,
    decoration: InputDecoration(
      hintText: 'Email',
      labelText: 'Email',
      helperText: 'Just set the Email',
      suffixIcon: Icon(Icons.alternate_email),
      icon: Icon(Icons.email),
      border: OutlineInputBorder(
        borderRadius: BorderRadius.circular(20.0)
      )
    ),
    onChanged: (value) {
      setState(() {
      });
    },
  );
  Widget _createPasswordInput() => TextField(
    controller: personForm['password'],
    obscureText: true,
    decoration: InputDecoration(
      hintText: 'Password',
      labelText: 'Password',
      helperText: 'Just set the Password',
      suffixIcon: Icon(Icons.visibility_off),
      icon: Icon(Icons.lock),
      border: OutlineInputBorder(
        borderRadius: BorderRadius.circular(20.0)
      )
    ),
    onChanged: (value) {
      setState(() {
      });
    },
  );

  Widget _createPerson({
    String name, 
    String email = "damcar135@gmail.com", 
    String password ="",
    bool preview = true
  }) {
    // Calculate avatar string

    String avatar = "";
    if (name.length != null) {
      List<String> explodedNames = name.split(' ');
      int  initials = 0;
      for (int i = 0 ; i< explodedNames.length; i++) {
        final explodedName = explodedNames[i];
        if (initials == 2) {
          break;
        }
        if (explodedName != ""){
          avatar += explodedName[0].toUpperCase();
          initials++;
        }
      }
    }
    List<Widget> subtitle = [];

    if (email != "") {
      subtitle.add(SizedBox(height:10.0));
      subtitle.add(Row(
        children: [
          Icon(Icons.email),
          SizedBox(width: 10.0,),
          Flexible(
            child: new Container(
              child: new Text(
                email,
                overflow: TextOverflow.ellipsis,
              ),
            ),
          )
          
        ],
      ));
    }

    if (password.isNotEmpty) {
      subtitle.add(SizedBox(height:10.0));
      subtitle.add(Row(
        children: [
          Icon(Icons.lock),
          SizedBox(width: 10.0,),
          Text('Secured by password')
        ],
      ));
    }

    Widget saveOrEditButton;
    Widget cancelOrEditButton;
    // set the buttons for preview
    if (preview) {
      //ask if editing
      saveOrEditButton =  FlatButton(
        onPressed: (){
          // add the actual data to the
          setState(() {
            users.add(
              {
                'email': email, 
                'name': name, 
                'password': password
              }
            );
            clearInputs();
            // add alert

          });
        },
        child: Row(
          children: [
            Icon(
              Icons.add,
              color: Colors.tealAccent[400]
            ),
            Text(
              'Add',
              style: TextStyle(
                color: Colors.tealAccent[400]
              ),
            )
          ],
        ),
      );
      cancelOrEditButton = FlatButton(
        onPressed: clearInputs,
        child: Row(
          children: [
            Icon(
              Icons.close,
              color: Colors.red,
            ),
            Text(
              'Cancel', 
              style: TextStyle(
                color: Colors.red
              ),
            )
          ],
        ),
      );
    } else {
      saveOrEditButton = FlatButton(
        onPressed: (){
          // add the actual data to the

        },
        child: Row(
          children: [
            Icon(
              Icons.edit,
              color: Colors.yellowAccent[400]
            ),
            Text(
              'Edit',
              style: TextStyle(
                color: Colors.yellowAccent[400]
              ),
            )
          ],
        ),
      );
      cancelOrEditButton = FlatButton(
        onPressed: clearInputs,
        child: Row(
          children: [
            Icon(
              Icons.delete,
              color: Colors.red,
            ),
            Text(
              'Delete', 
              style: TextStyle(
                color: Colors.red
              ),
            )
          ],
        ),
      );
    }


    final customCard = Container(
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: ListTile(
              leading: CircleAvatar(
                  child: Text(avatar),
                  backgroundColor: Colors.red,
              ),
              title: Flexible(
                child: new Container(
                  child: new Text(
                    name,
                    overflow: TextOverflow.ellipsis,
                    style: new TextStyle(
                      fontSize: 13.0,
                      fontFamily: 'Roboto',
                      color: new Color(0xFF212121),
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ),
              subtitle: Column(
                children: subtitle,
              ),
            ),
          ),
          // set edit and remove buttons
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                saveOrEditButton,
                cancelOrEditButton
              ]
            ),
          )
        ],
      )
    );
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20.0),
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            color: Colors.black26,
            blurRadius: 10.0,

          )
        ]
      ),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(20),
        child: customCard
      ),
    );
  }

  List<Widget> _renderUsers() {
    List<Widget> output = [];
    users.map((user) => _createPerson(
        preview: false, 
        email: user['email'], 
        name: user['name'], 
        password: user['password']
      )
    ).toList().forEach((element) { 
      output.add(element);
      output.add(SizedBox(height:20.0));
    });
    return output;
  }
}