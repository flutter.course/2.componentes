import 'package:flutter/material.dart';

class AlertsPage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Alerts'),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.arrow_left_rounded),
        onPressed: (){
          Navigator.pop(context);
        },
      ),
      body: Center(
        child: RaisedButton(
          child: Text('Show alert'),
          color: Colors.blue,
          textColor: Colors.white,
          shape: StadiumBorder(),
          onPressed: () =>_showAlert(context),
        ),
      ),
    );
  }

  _showAlert(BuildContext context ) {
    showDialog(
      context: context,
      barrierDismissible: true, 
      builder: (context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10.0)
          ),
          title: Text('Title'),
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Text('Content from alert box'),
              FlutterLogo( size: 100.0),
            ],
          ),
          actions: [
            FlatButton(
              onPressed: () => Navigator.of(context).pop(), 
              child: Text('Cancel')  
            ),
            FlatButton(
              onPressed: () => Navigator.of(context).pop(), 
              child: Text('Ok') 
            )
          ],
        );
      }
    );
  }
}