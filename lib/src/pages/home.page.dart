import 'package:flutter/material.dart';
import 'package:componentes/src/providers/menu.provider.dart';
import 'package:componentes/src/utils/string.icon.loader.dart';


class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Componentes'),
      ),
      body: _list(),
    );
  }

  Widget _list() {
    //menuProvider.cargarData();
    return FutureBuilder(
      future: menuProvider.cargarData(),
      initialData: [],
      builder: ( context, AsyncSnapshot<List<dynamic>> snapshot) {
        return ListView(
          children: _listItems(snapshot.data, context)
        );
      },
    );
  }

  List<Widget> _listItems(List<dynamic> data, BuildContext context) {
    if (data == null) {
      return []; 
    }
    return  data.map(
      (e) => Column(
        children: [
          ListTile(
            title: Text(e['texto']),
            leading: getIcon(e['icon']),
            trailing: Icon(
              Icons.keyboard_arrow_right_rounded
            ),
            onTap: () {
              Navigator.pushNamed(context, e['ruta']);
            },
          ),
          Divider()
        ],
      )).toList();
  }
}