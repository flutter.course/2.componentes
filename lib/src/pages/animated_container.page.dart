import 'package:flutter/material.dart';

class AnimatedContainerPage extends StatefulWidget {
  @override
  _AnimatedContainerPageState createState() => _AnimatedContainerPageState();
}

class _AnimatedContainerPageState extends State<AnimatedContainerPage> {



  int shapeIndex = 0;
  final List<Map<String,dynamic>> shapes = [
    {
      'width': 50.0,
      'height': 50.0,
      'color': Colors.pink,
      'geometry': BorderRadius.circular(8)
    },
    {
      'width': 10.0,
      'height': 50.0,
      'color': Colors.purpleAccent,
      'geometry': BorderRadius.circular(0)
    },
    {
      'width': 90.0,
      'height': 50.0,
      'color': Colors.red,
      'geometry': BorderRadius.circular(20)
    },
    {
      'width': 200.0,
      'height': 100.0,
      'color': Colors.teal,
      'geometry': BorderRadius.circular(20)
    }
  ];




  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Animated Container'),
      ),
      body: Center(
        child: AnimatedContainer(
          duration: Duration(seconds: 1),
          curve: Curves.fastOutSlowIn,
          width: shapes[shapeIndex]['width'],
          height: shapes[shapeIndex]['height'],
          decoration: BoxDecoration(
            borderRadius: shapes[shapeIndex]['geometry'],
            color: shapes[shapeIndex]['color']
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: (){
          setState(() {
            shapeIndex = shapeIndex < shapes.length-1? shapeIndex +1 : 0;
          });
        },
        child: Icon(Icons.play_arrow),
      ),
    );
  }
}