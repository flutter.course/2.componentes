import 'package:flutter/material.dart';

class HomePageTemp extends StatelessWidget {
  final options = ['uno','dos','tres','cuatro','cinco'];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Componentes Temp")
      ),
      body: ListView(
        //children: _generateItems(),
        children: _generateItemsShort(),
      ),
    );
  }

  List<Widget> _generateItems(){
    List<Widget> x = [];
    for (var option in options) {
      x..add(ListTile( title: Text(option)))
        ..add(Divider());
    }
    return x;
  } 

  List<Widget> _generateItemsShort(){
    return  options.map((option){
      return Column(
        children: [
          ListTile(
            title: Text(option + '!'),
            subtitle: Text('Numero $option'),
            leading: Icon(Icons.account_circle_rounded),
            trailing: Icon(Icons.arrow_right_outlined),
            onTap: (){},
          ),
          Divider()
        ],
      );
    }).toList();
  }
}