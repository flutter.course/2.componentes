import 'package:flutter/material.dart';

class CardsPage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Cards'),
      ),
      body: ListView(
        padding: EdgeInsets.all(10.0),
        children: <Widget>[
          _cardTypeCustom(),
          SizedBox(height: 30.0),
          _cardTypeOne(),
          SizedBox(height: 30.0),
          _cardTypeTwo(),

        ],
      ),
    );
  }


  _cardTypeOne() {
    return Card(
      elevation: 10.0,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20.0) 
      ),
      child: Column(
        children: [
          ListTile(
            leading: Icon(Icons.photo_album, color: Colors.blue,),
            title: Text('Lorem Ipsum'),
            subtitle:Text('lorem ipsum doloret sit amet'),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              FlatButton(onPressed: (){}, child: Text('Cancelar')),
              FlatButton(onPressed: (){}, child: Text('Ok'))
            ],
          )
        ],
      ),
    );
  }

  _cardTypeTwo() {
    return Card(
      clipBehavior: Clip.antiAlias,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20)
      ),
      child: Column(
        children: [
          // Image(
          //   image: NetworkImage('https://cdnb.artstation.com/p/assets/images/images/003/733/761/large/mark-kirkpatrick-mk-landscape-01.jpg?1599194319')
          // ),
          FadeInImage(
            image:NetworkImage('https://cdnb.artstation.com/p/assets/images/images/003/733/761/large/mark-kirkpatrick-mk-landscape-01.jpg?1599194319') , 
            placeholder: AssetImage('assets/loading.gif'),
            fadeInDuration: Duration(milliseconds: 200),
            height: 300.0,
            fit: BoxFit.cover,
          ),
          Container(
            padding: EdgeInsets.all(10.0),
            child: Text('Image'),
          )
          
        ],
      )
    );
  }

  // Card type created by myself as an excercise
  _cardTypeCustom() {
    final customCard =  Container(
      child: Column(
        children: [
          // set an image
          FadeInImage(
            image:NetworkImage('https://cdnb.artstation.com/p/assets/images/images/003/733/761/large/mark-kirkpatrick-mk-landscape-01.jpg?1599194319') , 
            placeholder: AssetImage('assets/loading.gif'),
            fadeInDuration: Duration(milliseconds: 200),
            height: 300.0,
            fit: BoxFit.cover,
          ),
          // set text
          
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: ListTile(
              title: Text('Custom Card'),
              subtitle: Text('Card made by myself using just containers and stuff taking some examples from the Flutter course from udemy imparted by Fernando Herrera'),
            ),
          ),
          // set edit and remove buttons
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                FlatButton(
                  onPressed: (){},
                  child: Row(
                    children: [
                      Icon(
                        Icons.edit,
                        color: Colors.yellowAccent[700]
                      ),
                      Text(
                        'Edit',
                        style: TextStyle(
                          color: Colors.yellowAccent[700]
                        ),
                      )
                    ],
                  ),
                ),
                FlatButton(
                  onPressed: (){},
                  child: Row(
                    children: [
                      Icon(
                        Icons.delete,
                        color: Colors.red,
                      ),
                      Text(
                        'Delete', 
                        style: TextStyle(
                          color: Colors.red
                        ),
                      )
                    ],
                  ),
                ),
              ]
            ),
          )
        ],
      )
    );
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20.0),
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            color: Colors.black26,
            blurRadius: 10.0,

          )
        ]
      ),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(20),
        child: customCard
      ),
    );
  }
}